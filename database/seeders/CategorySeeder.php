<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data for Category (Level 1)
        $categories = [
            'Laptop',
            'Điện Thoại',
            'Tablet',
            'Phụ Kiện',
            'Smartwatch',
        ];

        foreach ($categories as $categoryName) {
            try {
                // Insert into table categories
                Category::create([
                    'name' => $categoryName,
                    'admin_id' => rand(1, 3),
                ]);
            
                echo "Inserted ($categoryName): Passed";
                echo PHP_EOL; // new line
            } catch (Exception $exception) {
                echo 'Insert '. $categoryName .': Failed - Error Message: ' . $exception->getMessage();
                echo PHP_EOL; // new line
            }
        }
    }
}
