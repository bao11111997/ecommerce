<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);

        $this->call(RoleSeeder::class);

        $this->call(AdminSeeder::class);

        $this->call(CategorySeeder::class);

        $this->call(BrandSeeder::class);

        $this->call(ProductSeeder::class);

        $this->call(CommentSeeder::class);
        
        $this->call(OrderSeeder::class);
    }
}
