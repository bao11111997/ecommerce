<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'user',
            'bao',
            'nhi',
            'hoa',
            'man',
            'dao',
            'cuc',
            'tung',
            'truc',
            'mai',
            'thuy',
            'hien',
            'trinhbao456',
        ];
        $i = 1;
        foreach ($name as $value) {
            User::create([
                'id'                => $i,
                'name'              => $value,
                'email'             => $value.'@gmail.com',
                'address'           => '89 Quang Trung',
                'phone'             => '123456789',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'remember_token'    => Str::random(10),
            ]);
            $i++;
        }
        
    }
}
