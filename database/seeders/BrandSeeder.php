<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            'Samsung',
            'Apple',
            'Realme',
            'Dell',
            'MSI',
        ];

        foreach ($brands as $brand) {
            try {
                // Insert into table categories
                Brand::create([
                    'name' => $brand,
                    'admin_id' => rand(1, 3),
                ]);
            
                echo "Inserted ($brand): Passed";
                echo PHP_EOL; // new line
            } catch (Exception $exception) {
                echo 'Insert '. $brand .': Failed - Error Message: ' . $exception->getMessage();
                echo PHP_EOL; // new line
            }
        }
    }
}
