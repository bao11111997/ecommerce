<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;


class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $users = User::all();
        $products = Product::all();

        // Total
        $total = $products->count();

        if (!empty($users) && !empty($products)) {
            foreach ($users as $user) {
                // Random an array key
                $arrayKeys = range(0, ($total - 1));

                $order = Order::create([
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'phone' => $user->phone,
                    'address' => $user->address,
                    'status' => rand(1, 4),
                    'comment' => $faker->text,
                ]);

                // Get random key
                $randKeys = array_rand($arrayKeys, 2);

                // Get Product from collection $products
                $orderDetail = [];

                foreach ($randKeys as $key) {
                    $product = $products[$key];

                    // Define Variable $quantity
                    $quantity = random_int(1, 2);

                    $orderDetail[] = new OrderDetail([
                            'product_id' => $product->id,
                            'price' => $product->price,
                            'quantity' => $quantity,
                        ]);
                }
                $order->orderDetails()->saveMany($orderDetail);
            }
        }
    }
}
