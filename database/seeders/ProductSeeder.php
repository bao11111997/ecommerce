<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'        => 'Điện Thoại Samsung Galaxy Z Flip4',
                'category_id' => 2,
                'brand_id'    => 1,
                'price'       => '20990000',
                'content'     => 'Samsung Galaxy Z Flip4 128GB đã chính thức ra mắt thị trường công nghệ, đánh dấu sự trở lại của Samsung trên con đường định hướng người dùng về sự tiện lợi trên những chiếc điện thoại gập. Với độ bền được gia tăng cùng kiểu thiết kế đẹp mắt giúp Flip4 trở thành một trong những tâm điểm sáng giá cho nửa cuối năm 2022.',
            ],
            [
                'name'        => 'Điện Thoại Samsung Galaxy S22 Ultra 5G',
                'category_id' => 2,
                'brand_id'    => 1,
                'price'       => '25990000',
                'content'     => 'Galaxy S22 Ultra 5G chiếc smartphone cao cấp nhất trong bộ 3 Galaxy S22 series mà Samsung đã cho ra mắt. Tích hợp bút S Pen hoàn hảo trong thân máy, trang bị vi xử lý mạnh mẽ cho các tác vụ sử dụng vô cùng mượt mà và nổi bật hơn với cụm camera không viền độc đáo mang đậm dấu ấn riêng.',
            ],
            [
                'name'        => 'Điện Thoại iPhone 13 Pro Max',
                'category_id' => 2,
                'brand_id'    => 2,
                'price'       => '28490000',
                'content'     => 'Điện thoại iPhone 13 Pro Max 128 GB - siêu phẩm được mong chờ nhất ở nửa cuối năm 2021 đến từ Apple. Máy có thiết kế không mấy đột phá khi so với người tiền nhiệm, bên trong đây vẫn là một sản phẩm có màn hình siêu đẹp, tần số quét được nâng cấp lên 120 Hz mượt mà, cảm biến camera có kích thước lớn hơn, cùng hiệu năng mạnh mẽ với sức mạnh đến từ Apple A15 Bionic, sẵn sàng cùng bạn chinh phục mọi thử thách.',
            ],
            [
                'name'        => 'Điện Thoại iPhone 14 Pro Max',
                'category_id' => 2,
                'brand_id'    => 2,
                'price'       => '33990000',
                'content'     => 'Cuối cùng thì chiếc iPhone 14 Pro Max cũng đã chính thức lộ diện tại sự kiện ra mắt thường niên vào ngày 08/09 đến từ nhà Apple, kết thúc bao lời đồn đoán bằng một bộ thông số cực kỳ ấn tượng cùng vẻ ngoài đẹp mắt sang trọng. Từ ngày 14/10/2022 người dùng đã có thể mua sắm các sản phẩm iPhone 14 series với đầy đủ phiên bản tại Thế Giới Di Động.',
            ],
            [
                'name'        => 'Điện Thoại Realme 9 Pro+ 5G',
                'category_id' => 2,
                'brand_id'    => 3,
                'price'       => '9990000',
                'content'     => 'Realme 9 Pro+ 5G - chiếc smartphone tầm trung của Realme đã được ra mắt, máy có một thiết kế đầy màu sắc, cụm 3 camera với cảm biến IMX766 giúp bạn có được những bức ảnh sinh động.',
            ],
            [
                'name'        => 'Điện Thoại Realme 8 Pro',
                'category_id' => 2,
                'brand_id'    => 3,
                'price'       => '8690000',
                'content'     => 'Bên cạnh Realme 8, Realme 8 Pro cũng được giới thiệu đến người dùng với điểm nhấn chính là sự xuất hiện của camera 108 MP siêu nét cùng công nghệ sạc SuperDart 50 W và đi kèm mức giá bán tầm trung rất lý tưởng.',
            ],
            [
                'name'        => 'Laptop Dell Gaming G15 5515 R5',
                'category_id' => 1,
                'brand_id'    => 4,
                'price'       => '23490000',
                'content'     => 'Bộ hiệu năng gây ấn tượng đến từ con chip AMD mạnh mẽ cùng thiết kế cá tính, nổi bật, laptop Dell Gaming G15 5515 R5 (P105F004DGR) sẽ đáp ứng tối ưu mọi nhu cầu từ các tác vụ văn phòng cơ bản đến những ứng dụng đồ họa, chơi game giải trí chuyên nghiệp.',
            ],
            [
                'name'        => 'Laptop Dell Inspiron 15 3511 i3',
                'category_id' => 1,
                'brand_id'    => 4,
                'price'       => '12390000',
                'content'     => 'Laptop Dell Inspiron 15 3511 i3 (P112F001CBL) sở hữu thiết kế sang trọng, thanh lịch với sức mạnh hiệu năng đến từ dòng chip Intel thế hệ thứ 11 đáp ứng tốt các tác vụ học tập, văn phòng và giải trí cơ bản của người dùng học sinh, sinh viên.',
            ],
            [
                'name'        => 'Laptop Dell Vostro 3510 i5',
                'category_id' => 1,
                'brand_id'    => 4,
                'price'       => '20490000',
                'content'     => 'Laptop Dell Vostro 3510 i5 (P112F002BBL) sở hữu cấu hình mạnh mẽ, đa nhiệm mượt mà trên sức mạnh của bộ vi xử lý Intel thế hệ 11, cùng một thiết kế đơn giản mà sang đẹp, sẽ là lựa chọn đắt giá đáp ứng nhu cầu học tập, làm việc hay giải trí của bạn.',
            ],
            [
                'name'        => 'Laptop Dell Gaming G15 5511 i5',
                'category_id' => 1,
                'brand_id'    => 4,
                'price'       => '26210000',
                'content'     => 'Không những mang đến cho người dùng hiệu năng ấn tượng nhờ con chip Intel thế hệ 11 tân tiến, laptop Dell Gaming G15 5511 i5 11400H (70266676) còn sở hữu thiết kế thời thượng, lôi cuốn, hứa hẹn sẽ là người cộng sự lý tưởng cùng bạn chinh phục mọi chiến trường.',
            ],
            [
                'name'        => 'Laptop Dell Vostro 5410 i5',
                'category_id' => 1,
                'brand_id'    => 4,
                'price'       => '21090000',
                'content'     => 'Sở hữu phong cách thiết kế thanh lịch, tinh tế cùng những thông số kỹ thuật khá ấn tượng cho đa dạng nhu cầu từ laptop học tập - văn phòng đến giải trí cơ bản, laptop Dell Vostro 5410 i5 (V4I5214W1) sẽ là một lựa chọn mang lại sự hài lòng cho bạn.',
            ],
        ];

        $i = 1;

        foreach ($products as $key => $value) {
		$name = $products[$key]['name'];
            $product = Product::create([
                'name' => $name,
                'category_id' => $products[$key]['category_id'],
                'admin_id' => rand(1, 7),
                'brand_id' => $products[$key]['brand_id'],
                'thumbnail' => 'product/thumbnail/'.$i.'.jpg',
                'content' => $products[$key]['content'],
                'quantity' => 100,
                'price' => $products[$key]['price'],
            ]);
            for ($j=1; $j <= 3 ; $j++) { 
            
            $product->productImages()
            ->create([
                'url' => 'product/product_image/'.$i.'.'.$j.'.jpg',
                ]);
            }
            $i++;
        }
    }
}
