<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'id'                => 1,
            'name'              => 'superadmin',
            'email'             => 'superadmin@gmail.com',
            'email_verified_at' => now(),
            'phone'             => '123456789',
            'status'            => '1',
            'role_id'           => '1',
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
        ]);

        Admin::create([
            'id'                => 2,
            'name'              => 'trinhbao456',
            'email'             => 'trinhbao456@gmail.com',
            'email_verified_at' => now(),
            'phone'             => '123456789',
            'status'            => '1',
            'role_id'           => '2',
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
        ]);

        Admin::create([
            'id'                => 3,
            'name'              => 'admin',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => now(),
            'phone'             => '123456789',
            'status'            => '1',
            'role_id'           => '2',
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
        ]);

        $admins = [
            'bao',
            'nhi',
            'dat',
            'ctv',
        ];
        
        $i = 4;

        foreach ($admins as $admin) {
            Admin::create([
            'id'                => $i,
            'name'              => $admin,
            'email'             => $admin.'@gmail.com',
            'email_verified_at' => now(),
            'phone'             => '123456789',
            'status'            => '1',
            'role_id'           => '3',
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
            ]);

            $i++;
        }
        
    }
}
