<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Comment;
class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker  = \Faker\Factory::create();

        $users = User::all();
        $products = Product::all();

        if (!empty($users) && !empty($products)) {
            foreach ($users as $user) {
                foreach ($products as $product) {
                    Comment::create([
                        'user_id' => $user->id,
                        'product_id' => $product->id,
                        'comment' => $faker->text,
                    ]);
                    
                }
            }
        }
    }
}
