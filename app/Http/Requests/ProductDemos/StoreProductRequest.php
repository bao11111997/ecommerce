<?php

namespace App\Http\Requests\ProductDemos;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'product_thimbnail' => 'required|image',
            'category_id' => 'required|exists:categories,id',
            'product_price' => 'required',
            'product_quantity' => 'required',
            'product_content' => 'required',
        ];
    }
}
