<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use App\Util\UserCommon;

class UserController extends Controller
{
    function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data  = [];

        $users = $this->user->with('orders');

        // Check if have search Email
        if (!empty($request->get('email'))) {
            $users->where('email' , $request->get('email'));
        }

        // Check if have search Customer ID
        if (!empty($request->get('customer_id'))) {
            $users->where('id' , $request->get('customer_id'));
        }
        
        // Check if have search Name
        if (!empty($request->get('name'))) {
            $users->where('name' , 'like', '%'.$request->get('name').'%');
        }
        
        // Check if have search Phone
        if (!empty($request->get('phone'))) {
            $users->where('phone' , $request->get('phone'));
        }
        
        // Check if have search Address
        if (!empty($request->get('address'))) {
            $users->where('address' , 'like', '%'.$request->get('address').'%');
        }

        // Get list users
        $users         = $users->orderBy('id', 'desc')->paginate(10);

        $data['users'] = $users;
        return view('backend.users.index', $data);
    }

    public function userByDate()
    {
        $data = [];

        $year = Carbon::now()->year;

        $data['year'] = $year;
        return view('backend.users.user_by_date', $data);
    }

    public function searchUserByDate(Request $request)
    {

        $day   = $request->get('day');
        $month = $request->get('month');
        $year  = $request->get('year');

        $users = $this->user->with('orders.orderDetails');

        $users->whereYear('created_at', $year);

        $status = 'year';
        if (!empty($month)) {

            $users->whereMonth('created_at', $month);
            $status = 'month';

            if (!empty($day)) {
                $users->whereDay('created_at', $day);
                $status = 'day';
            }
        }
        
        $users = $users->orderBy('created_at', 'asc')->get();

        if ($status == 'year') {
            $day['year'] = $year;

            $result = UserCommon::calculate($users, $day, 'mon');
            
            $users  = $result;
        }

        if ($status == 'month') {
            $day['year']  = $year;
            $day['month'] = $month;

            $result = UserCommon::calculate($users, $day, 'mday');
            
            $users  = $result;
        }

        $date = [];
        if ($status == 'day') {
            $date['year']  = $year;
            $date['month'] = $month;
            $date['day']   = $day;

            $data['date']  = $date; 
        }

        $data['users']  = $users;
        $data['status'] = $status;
        return view('backend.users.search_user_by_day', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        
        // Get User
        $user = $this->user->with('orders')->findOrFail($id);
        // dd($user);
        $data['user'] = $user;
        return view('backend.users.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
