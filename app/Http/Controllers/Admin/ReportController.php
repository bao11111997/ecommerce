<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Category;
use App\Models\Brand;
use App\Models\OrderDetail;
use App\Util\OrderCommon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    function __construct(Order $order, OrderDetail $orderDetail)
    {
        $this->order       = $order;
        $this->orderDetail = $orderDetail;
    }

    public function creditReport(Request $request)
    {
        $data = [];

        if (!empty($request->get('start')) && !empty($request->get('end'))) {
            $request ->validate([
                'start' => 'date',
                'end' => 'date'
            ]);

            $start = date('Y-m-d', strtotime($request->get('start')));

            $end   = date('Y-m-d', strtotime($request->get('end')) + 86400);

            if ($start <= $end) {
                $orders = $this->order->with('orderDetails')->whereBetween('created_at', [$start, $end])->get();

                $data['orders'] = $orders;
            }else {
                return view('backend.orders.credit')->with('error', 'Check the date again');
            }
        }
        
        return view('backend.orders.credit', $data);
    }

    public function summaryReport(Request $request)
    {
        $data       = [];

        $categories = Category::all();
        $brands     = Brand::all();

        if (!empty($request->get('start')) && !empty($request->get('end'))) {
            $request ->validate([
                'start' => 'date',
                'end' => 'date'
            ]);

            $start = date('Y-m-d', strtotime($request->get('start')));

            $end   = date('Y-m-d', strtotime($request->get('end')) + 86400);

            if ($start <= $end) {
                $orderDetails = $this->orderDetail
                ->with('product')
                ->whereBetween('created_at', [$start, $end])
                ->select('product_id', DB::raw('SUM(price*quantity) as price, SUM(quantity) as quantity'))
                ->groupBy('product_id');

                if (!empty($request->category)) {
                    $category = $request->category;

                    $orderDetails->whereHas('product', function (Builder $query) use ($category) {
                        $query->whereIn('category_id', $category);
                    });
                }

                if (!empty($request->brand)) {
                    $brand = $request->brand;

                    $orderDetails->whereHas('product', function (Builder $query) use ($brand) {
                        $query->whereIn('brand_id', $brand);
                    });
                }

                $orderDetails = $orderDetails->get();           
            }
            $data['orderDetails'] = $orderDetails;
            
        }

        $data['categories'] = $categories;
        $data['brands']     = $brands;
        return view('backend.orders.summary', $data);
    }

}
