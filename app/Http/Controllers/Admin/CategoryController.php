<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    function __construct(Category $category)
    {

        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data       = [];

        $categories = $this->category->with('products');

        if (!empty($request->search)) {
            $categories->where('name', 'like', '%'.$request->search.'%');
        }

        $categories         = $categories->get();

        $data['categories'] = $categories;
        return view('backend.categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $category = [
            'admin_id' => Auth::guard('admin')->user()->id,
            'name'     => $request->name,
        ];

        try {
            Category::create($category);

            return redirect(route('admin.categories.index'))->with('success', 'Thêm Mới Danh Mục Thành Công');
        } catch (Exception $e) {
            return back()->with('error', 'Thêm Mới Danh Mục Không Thành Công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data     = [];

        $category = $this->category->findOrFail($id);

        $data['category'] = $category;
        return view('backend.categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->category->findOrFail($id);

        $request->validate([
            'name' => 'required',
        ]);

        $category->name = $request->name;

        try {
            $category->save();

            return redirect(route('admin.categories.index'))->with('success', 'Cập Nhật Danh Mục Thành Công');
        } catch (Exception $e) {
            return back()->with('error', 'Cập Nhật Danh Mục Không Thành Công');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->category->findOrFail($id);

        try {
            // Delete data
            $category->delete();

            // Success
            return redirect()->route('admin.categories.index')
                ->with('success', 'Xóa Danh Mục Thành Công');
        } catch (\Exception $ex) {
            return redirect()->back()
                ->with('error', $ex->getMessage());
        }
    }
}
