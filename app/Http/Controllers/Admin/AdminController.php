<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Role;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Http\Requests\Admin\Auth\StoreRequest;
use App\Notifications\ResetPasswordRequest;

class AdminController extends Controller
{
    function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data   = [];

        $admins = $this->admin->with('role');
        
        // Search by Name
        if (!empty($request->get('name'))) {
            $admins->where('name', 'like', '%'.$request->get('name').'%')->orWhere('email', $request->get('name'));
        }

        // Search by Status
        if (!empty($request->get('active'))) {
            $admins->where('status', $request->get('active'));
        }

        $admins         = $admins->paginate(10);

        $data['admins'] = $admins;
        return view('backend.admins.index', $data);
    }

    public function login()
    {
        return view('backend.admins.login');
    }

    public function checkLogin(Request $request)
    {
        $validated = $this->validate($request, 
        [
            'email'    => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ]);
        
        // Check login
        if (Auth::guard('admin')->attempt($validated, $request->input('remember')) && Auth::guard('admin')->user()->active()) {
            // Create Session ID
            $request->session()->regenerate();

            return redirect()->route('admin.dashboard');
        }

        return redirect()->route('admin.login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect()->route('admin.login');
    }

    public function forgotPassword()
    {
        return view('backend.admins.forgot-password');
    }

    public function sendMail(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);
        $admin = Admin::where('email', $request->email)->firstOrFail();
        $passwordReset = PasswordReset::updateOrCreate([
            'email' => $admin->email,
        ],

            ['token' => Str::random(60),
        ]);

        if ($passwordReset) {
            $admin->notify(new ResetPasswordRequest($passwordReset->token, $passwordReset->email));
        }

        return back()->with('status', 'Chúng tôi đã gửi qua email liên kết đặt lại mật khẩu của bạn!');
    }

    public function resetPassword(Request $request)
    {
        $data  = [];

        $admin = PasswordReset::where('token', $request->get('token'))->where('email', $request->get('email'))->firstOrFail();

        $data['admin'] = $admin;
        return view('backend.admins.reset-password', $data);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|confirmed|min:8',
        ]);

        $password = Hash::make($request->password);

        // $admin = PasswordReset::where('token', $request->get('_token'))->firstOrFail();

        try {
            Admin::where('email', $request->email)->update(['password' => $password]);

            return redirect()->route('admin.login')->with('success', 'Đặt Lại Mật Khẩu Thành Công');

        } catch (Exception $e) {
            return redirect()->route('admin.login')->with('error', 'Đặt Lại Mật Khẩu Không Thành Công');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data  = [];

        $roles = Role::all();

        $data['roles'] = $roles;
        return view('backend.admins.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $admin = [
            'name'              => $request->name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'role_id'           => $request->role,
            'status'            => $request->status,
            'email_verified_at' => now(),
            'password'          => Hash::make($request->password),
        ];

        try {
            Admin::create($admin);

            return redirect(route('admin.index'))->with('success', 'Thêm Mới Thành Công');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Thêm Mới Thất Bại');
        }
    }

    public function profile($id)
    {
        $data = [];

        if (auth('admin')->user()->id == $id) {
            $admin = $this->admin->with('role')
                        ->with('products')
                        ->findOrFail($id);

            $data['admin'] = $admin;
            return view('backend.admins.profile', $data);
        }

        return redirect()->route('admin.dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  = [];

        $roles = Role::all();
        
        $admin = $this->admin->with('role')
            ->with('products')
            ->findOrFail($id);

        $data['admin'] = $admin;
        $data['roles'] = $roles;

        return view('backend.admins.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);

        if (!empty($request->name)) {
            $admin->name = $request->name;
        }

        if (!empty($request->phone)) {
            $admin->phone = $request->phone;
        }

        if (!empty($request->role_id)) {
            $admin->role_id = $request->role_id;
        }

        if (!empty($request->status)) {
            $admin->status = $request->status;
        }

        if (!empty($request->password)) {
            $request->validate([
                'password' => 'required|confirmed|min:8',
            ]);

            $admin->password = $request->password;
        }

        try {
            $admin->save();

            return back()->with('success', 'Thay Đổi Thông Tin Thành Công');
        } catch (Exception $e) {
            return back()->with('error', 'Thay Đổi Thông Tin Không Thành Công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
