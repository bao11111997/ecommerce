<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImage;
use App\Http\Requests\Admin\Product\StoreRequest;
use Illuminate\Support\Facades\Auth;
use App\Util\ProductCommon;

class ProductController extends Controller
{
    function __construct(Product $product)
    {

        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        
        $categories = Category::all();
        // dd($request->all());
        $products = $this->product->with('admin')->with('orderDetails')->orderBy('id', 'desc');

        if (!empty($request->search)) {
            $products->where('name', 'like', '%'.$request->search.'%');
        }
        if (!empty($request->category_id)) {
            $products->where('category_id', $request->category_id);
        }

        $products = $products->paginate(12);

        $data['categories'] = $categories;
        $data['products'] = $products;
        return view('backend.products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];

        $categories = Category::all();
        $brands     = Brand::all();

        $data['brands']     = $brands;
        $data['categories'] = $categories;
        return view('backend.products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        // Upload thumbnail
        $thumbnail = ProductCommon::uploadFile($request->thumbnail, 'product/thumbnail/');

        // Define Variable
        $product = [
            'admin_id' => Auth::guard('admin')->user()->id,
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'content' => $request->content,
            'thumbnail' => $thumbnail,
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,
        ];

        try {
            // Create product
           $product = Product::create($product);

           $product_images = ProductCommon::uploadMultipleFile($request->product_images, 'product/product_image/', $product->id);

           // Upload Multiple File
           foreach ($product_images as $product_image) {
               ProductImage::create($product_image);
           }
           
            // Success
            return redirect()->route('admin.products.index')
                ->with('success', 'Thêm mới sản phẩm thành công.');
        } catch (\Exception $ex) {
            return redirect()->back()
                ->with('error', $ex->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        $product = Product::with('category')
        ->with('brand')
        ->with('admin')
        ->with('productImages')
        ->findOrFail($id);

        $data ['product'] = $product;
        return view('backend.products.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];

        $categories = Category::all();
        $brands     = Brand::all();

        $product = Product::with('category')
                    ->with('brand')
                    ->with('admin')
                    ->with('productImages')
                    ->findOrFail($id);

        $data ['product']   = $product;
        $data['brands']     = $brands;
        $data['categories'] = $categories;
        return view('backend.products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Get Product
        $product = Product::with('category')
                    ->with('brand')
                    ->with('admin')
                    ->with('productImages')
                    ->findOrFail($id);

        $request->validate([
            'name' => 'required',
            'content' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
        ]);

        // Xóa Product Image
        if (!empty($request->check_images)) {
            $product_images = ProductImage::where('product_id', $product->id)->whereNotIn('id', $request->check_images)->get();
            foreach ($product_images as $product_image) {
                ProductCommon::removeFile($product_image->url);

                ProductImage::destroy($product_image->id);
            }    
        }else{
            $product_images = ProductImage::where('product_id', $product->id)->get();
            foreach ($product_images as $product_image) {
                ProductCommon::removeFile($product_image->url);

                ProductImage::destroy($product_image->id);
            }
        }
        
        // Upload Product Image
        if (!empty($request->product_images)) {
            $product_images = ProductCommon::uploadMultipleFile($request->product_images, 'product/product_image/', $product->id);

           // Upload Multiple File
           foreach ($product_images as $product_image) {
               ProductImage::create($product_image);
           }
        }

        if (!empty($request->thumbnail)) {
            ProductCommon::removeFile($product->thumbnail);
            $product->thumbnail = ProductCommon::uploadFile($request->thumbnail, 'product/thumbnail/');
        }

        $product->name = $request->name;
        $product->content = $request->content;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;

        try {
            $product->save();

            return redirect(route('admin.products.show', $product->id))->with('success', 'Cập Nhật Sản Phẩm Thành Công');
        } catch (Exception $e) {
            return back()->with('error', 'Cập Nhật Sản Phẩm Không Thành Công');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->with('orderDetails')
            ->findOrFail($id);

        if (count($product->orderDetails) === 0) {
            try {
                $product_images = ProductImage::where('product_id', $product->id)->get();
                foreach ($product_images as $product_image) {
                    ProductCommon::removeFile($product_image->url);

                    ProductImage::destroy($product_image->id);
                }

                ProductCommon::removeFile($product->thumbnail);
                // Delete data of table products
                $product->delete();

                // Success
                return redirect()->route('admin.products.index')
                    ->with('success', 'Xóa Thành Công Sản Phẩm');
            } catch (\Exception $ex) {
                return redirect()->back()
                    ->with('error', $ex->getMessage());
            }
        }else{
            return redirect(route('admin.products.show', $product->id))->with('error', 'Sản Phẩm Hiện Đang Có Đơn Hàng');
        }

        
    }
}
