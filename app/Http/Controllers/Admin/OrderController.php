<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Category;
use App\Models\Brand;
use App\Models\OrderDetail;
use App\Util\OrderCommon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    function __construct(Order $order, OrderDetail $orderDetail)
    {
        $this->order = $order;
        $this->orderDetail = $orderDetail;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data   = [];

        $orders = $this->order->with('user')->with('orderDetails');

        // Check if have search Order ID
        if (!empty($request->get('order_number'))) {
            $orders->where('id' , $request->get('order_number'));
        }

        // Check if have search Customer ID
        if (!empty($request->get('customer_id'))) {
            $orders->where('user_id' , $request->get('customer_id'));
        }
        
        // Check if have search Name
        if (!empty($request->get('name'))) {
            $orders->where('name' , 'like', '%'.$request->get('name').'%');
        }
        
        // Check if have search Phone
        if (!empty($request->get('phone'))) {
            $orders->where('phone' , $request->get('phone'));
        }
        
        // Check if have search Customer ID
        if (!empty($request->get('address'))) {
            $orders->where('address' , $request->get('address'));
        }
        
        // Lấy list order
        $orders         = $orders->orderBy('created_at', 'desc')->paginate(10);

        $data['orders'] = $orders;
        return view('backend.orders.index', $data);
    }

    public function orderByDate()
    {
        $data = [];

        $year = Carbon::now()->year;

        $data['year'] = $year;
        return view('backend.orders.order_by_date', $data);
    }

    public function searchOrderByDate(Request $request)
    {

        $day   = $request->get('day');
        $month = $request->get('month');
        $year  = $request->get('year');

        $orders = $this->order->with('user')->with('orderDetails');

        $orders->whereYear('created_at', $year);

        $status = 'year';
        if (!empty($month)) {

            $orders->whereMonth('created_at', $month);
            $status = 'month';

            if (!empty($day)) {
                $orders->whereDay('created_at', $day);
                $status = 'day';
            }
        }
        
        $orders = $orders->orderBy('created_at', 'asc')->get();

        if ($status == 'year') {
            $day['year'] = $year;

            $result = OrderCommon::calculate($orders, $day, 'mon');

            $orders = $result;
        }

        if ($status == 'month') {
            $day['year']  = $year;
            $day['month'] = $month;

            $result = OrderCommon::calculate($orders, $day, 'mday');

            $orders = $result;
        }

        $date = [];
        if ($status == 'day') {
            $date['year']  = $year;
            $date['month'] = $month;
            $date['day']   = $day;

            $data['date'] = $date; 
        }
        // dd($orders);

        $data['orders'] = $orders;
        $data['status'] = $status; 

        return view('backend.orders.search_order_by_day', $data);
    }

    public function pendingOrder()
    {
        $data   = [];
        
        $orders = $this->order->with('orderDetails')
                    ->where('status', 1)
                    ->get();

        $data['orders'] = $orders;

        return view('backend.orders.pending_order', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];

        // Get orders
        $order = $this->order
                ->with('orderDetails')
                ->with('user')
                ->findOrFail($id);

        // Get Order Detail
        $orderDetails = OrderDetail::with('product')->where('order_id', $order->id)->get();
        
        $data['order']        = $order;
        $data['orderDetails'] = $orderDetails;
        return view('backend.orders.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order         = $this->order->findOrFail($id);

        $order->status = $request->status;

        try {
            $order->save();

            return back()->with('success', 'Cập Nhật Đơn Hàng Thành Công');
        } catch (Exception $e) {
            return back()->with('err', 'Cập Nhật Đơn Hàng Không Thành Công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = $this->order->findOrFail($id);

        try {
            // Delete data
            $order->delete();

            // Success
            return back()
                ->with('success', 'Xóa Đơn Hàng Thành Công');
        } catch (\Exception $ex) {
            return back()
                ->with('error', $ex->getMessage());
        }
    }
}
