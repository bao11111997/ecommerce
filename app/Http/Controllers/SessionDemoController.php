<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSessionDemoRequest;
use Illuminate\Http\Request;

class SessionDemoController extends Controller
{
    /**
     * DATA SESSION SAMPLE 
     * 
     */
    // $cartDemos = [
    //     [
    //         'cart_product_id' => 10,
    //         'cart_quantity' => 20
    //     ],
    //     [
    //         'cart_product_id' => 2,
    //         'cart_quantity' => 50
    //     ],
    //     [
    //         'cart_product_id' => 5,
    //         'cart_quantity' => 100
    //     ],
    // ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        // Check Session Cart Demo
        if (session('cart_demos')) {
            $cartDemos = session('cart_demos');

            $data['cart_demos'] = $cartDemos;
        }

        // dd($data);
        return view('session_demos.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSessionDemoRequest $request)
    {
        // Validate OK thì lưu thông tin Product và Quantity vào trong Giỏ hàng
        // Khai báo các biến để lấy data từ Form Submit gửi lên
        $productId = $request->product_id;
        $quantity = $request->quantity;

        // Get Data from SESSION
        $cartDemos = session('cart_demos') ?? [];
        // $cartDemos = isset(session('cart_demos')) ? session('cart_demos') : [];

        if (!empty($cartDemos)) {
            foreach ($cartDemos as $key => $cart) {
                if ($cart['cart_product_id'] == $productId) {
                    $quantityUpdate = $cart['cart_quantity'] + $quantity;

                    // Update Data for key Quantity
                    $cartDemos[$key]['cart_quantity'] = $quantityUpdate;
                } else {
                    $newProduct = [
                        'cart_product_id' => $productId,
                        'cart_quantity' => $quantity,
                    ];
                    array_push($cartDemos, $newProduct); // you can use case 1
                    // $cartDemos[] = $newProduct; // or you can use case 2
                }
            }
        } else {
            $newProduct = [
                'cart_product_id' => $productId,
                'cart_quantity' => $quantity,
            ];
            array_push($cartDemos, $newProduct); // you can use case 1
            // $cartDemos[] = $newProduct; // or you can use case 2
        }
        
        // Set data for SESSION
        session(['cart_demos' => $cartDemos]);

        return redirect()->route('session-demo.index')
            ->with('success', 'Thêm sản phẩm vào giỏ hàng thành công.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Get quantity from Request Form Submit
        $cartQuantity = $request->quantity;

        // Get Data from SESSION
        $cartDemos = session('cart_demos') ?? [];
        // $cartDemos = isset(session('cart_demos')) ? session('cart_demos') : [];

        if (!empty($cartDemos)) {
            foreach ($cartDemos as $key => $cart) {
                if ($cart['cart_product_id'] == $id) {
                    // Update Data for key Quantity
                    $cartDemos[$key]['cart_quantity'] = $cartQuantity;
                }
            }
        }

        // Set data for SESSION
        session(['cart_demos' => $cartDemos]);

        // Return JSON
        return response()->json([
            'success' => 'Update quantity thành công.',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get Data from SESSION
        $cartDemos = session('cart_demos') ?? [];
        // $cartDemos = isset(session('cart_demos')) ? session('cart_demos') : [];

        if (!empty($cartDemos)) {
            foreach ($cartDemos as $key => $cart) {
                if ($cart['cart_product_id'] == $id) {
                    // Remove Product
                    unset($cartDemos[$key]);
                }
            }

            // Set data for SESSION
            session(['cart_demos' => $cartDemos]);

            return redirect()->route('session-demo.index')
                ->with('success', 'Xoá sản phẩm ra khỏi giỏ hàng thành công.');
        }
        
        return redirect()->route('session-demo.index')
            ->with('error', 'Chưa có sản phẩm để xoá.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
        // Set data for SESSION
        session(['cart_demos' => []]);

        return redirect()->route('session-demo.index')
            ->with('success', 'Xoá tất cả các sản phẩm ra khỏi giỏ hàng thành công (giỏ hàng trống rỗng).');
    }
}
