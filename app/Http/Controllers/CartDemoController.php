<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartDemos\CheckQuantityRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class CartDemoController extends Controller
{
    public function index()
    {
        $data = [];

        $cartDemos = session('cart_demos');

        $data['cartDemos'] = $cartDemos;

        // dd($cartDemos);

        return view('cart_demos.index', $data);
    }

    public function addCart($id, Request $request)
    {
        // Get Product
        $product = Product::findOrFail($id);

        // Get quantity in Database and check quantity available ?
        $quantity = $product->quantity;
        if ($quantity < 0) {
            return back()
                ->with('error', 'Sản phẩm này đã hết hàng. Vui lòng chọn một sản phẩm khác');
        }

        // Get Data from SESSION
        $cartDemos = session('cart_demos') ?? [];
        // <====> $carts = isset(session('carts')) ? session('carts') : [];

        /**
         * Check Product exist in Cart ?
         *
         * True: increase the Quantity by 1
         * False: set Quantity by 1
         */
        if (!empty($cartDemos)) {
            foreach ($cartDemos as $key => $cart) {
                if ($cart['cart_product_id'] == $product->id) {
                    // Increase the Quantity by 1
                    $quantityUpdate = $cart['cart_quantity'] + 1;

                    // Update Data for key Quantity
                    $cartDemos[$key]['cart_quantity'] = $quantityUpdate;
                } else {
                    $newProduct = [
                        'cart_product_id' => $product->id,
                        'cart_quantity' => 1,
                        'cart_product_info' => $product,
                    ];
                    array_push($cartDemos, $newProduct); // you can use case 1
                    // <===> $cartDemos[] = $newProduct; // or you can use case 2
                }
            }
        } else {
            $newProduct = [
                'cart_product_id' => $product->id,
                'cart_quantity' => 1,
                'cart_product_info' => $product,
            ];
            array_push($cartDemos, $newProduct); // you can use case 1
            // <===> $cartDemos[] = $newProduct; // or you can use case 2
        }

        // Update Data for SESSION
        session(['cart_demos' => $cartDemos]);

        return redirect()->route('cart-demo.index')
                ->with('success', 'Thêm sản phẩm vào giỏ hàng thành công.');
    }

    public function updateQuantity(int $productId, CheckQuantityRequest $request)
    {
        // Get cart info from Session
        $cartDemos = session('cart_demos');

        // Check Avaialble $carts
        if ($cartDemos == null) {
            return redirect()->route('product-demo.index');
        }

        // Get quantity from Form Request
        $quantity = $request->quantity;

        // Get product fron Database
        $product = Product::findOrFail($productId);

        // Get Quantity from Database
        $quantityDB = $product->quantity;

        /**
         * Check if Quantity Form Request > quantityDB
         *
         * @if   true then show error message
         * @else then show success message
         */
        if ($quantity > $quantityDB) {
            return redirect()->route('cart-demo.index')->with('error', 'Vượt tồn kho. Vui lòng giảm số lượng.');
        }

        // dd($quantity);

        // Convert $carts from array to collection
        $collectionCartDemos = collect($cartDemos)->map(function ($cartDemo) use ($productId, $quantity) {
            if ($cartDemo['cart_product_id'] == $productId) {
                // Update value for attribute quantity
                $cartDemo['cart_quantity'] = $quantity;
            }

            // Return
            return $cartDemo;
        });

        // Convert data from collection to array
        $cartDemos = $collectionCartDemos->toArray();

        // Update Data for SESSION
        session(['cart_demos' => $cartDemos]);

        return redirect()->route('cart-demo.index')
            ->with('success', 'Đã cập nhật quantity cho sản phẩm thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeProductInCart($id)
    {
        // Get Product Info
        $product = Product::findOrFail($id);

        // Get cart info
        $cartDemos = session('cart_demos');

        // Check and Remove Product ID
        if (!empty($cartDemos)) {
            foreach ($cartDemos as $key => $value) {
                if ($value['cart_product_id'] == $product->id) {
                    unset($cartDemos[$key]);
                }
            }
        }

        // Update Data for SESSION
        session(['cart_demos' => $cartDemos]);

        return redirect()->route('cart-demo.index')
            ->with('success', 'Đã xoá sản phẩm ra khỏi giỏ hàng thành công.');
    }

}
