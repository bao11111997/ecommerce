<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductDemos\StoreProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductDemoController extends Controller
{
    public function index(){
        $data = [];
        $products = Product::latest()->paginate(10);
        $data['product'] = $products;
        return view('product_demos.index',$data);
    }

    public function create(){
        $data = [];
        //Get category list
        $categories = Category::pluck('name','id')->toArray();
        $data['categories'] = $categories;
        return view('product_demos.create',$data);
    }

    public function store(StoreProductRequest $request){
        // Upload Thumbnail
        // Get Extension (.png, jpg, jpeg, ...)
        $extension = $request->product_thumbnail->extension();

        // Convert string to lowercase
        $extension = strtolower($extension);

        // Set fileName
        $fileName = time() . '.' . $extension;

        // Upload file to server
        $request->product_thumbnail->move(storage_path('app/public/product_demos'), $fileName);

        // Get filePath
        $thumbnailPath = '/storage/product_demos/' . $fileName;

        // Define Variable
        $dataInsert = [
            'name' => $request->product_name,
            'thumbnail' => $thumbnailPath,
            'content' => $request->product_content,
            'quantity' => $request->product_quantity,
            'price' => $request->product_price,
            'category_id' => $request->category_id,
        ];

        try {
            // Insert into table products
            Product::create($dataInsert);

            // Success
            return redirect()->route('product-demo.index')
                ->with('success', 'Thêm mới sản phẩm thành công.');
        } catch (\Exception $ex) {
            return redirect()->back()
                ->with('error', $ex->getMessage())
                ->withInput();
        }
    }
}
