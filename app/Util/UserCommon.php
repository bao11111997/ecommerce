<?php 
namespace App\Util;

/**
 * 
 */
class UserCommon
{
	public static function calculate($users, $day, $key)
	{
		foreach ($users as $user) {
                $time = strtotime($user->created_at);
                $dt = getdate($time);
              
                if (isset($day[$dt[$key]]))
                {
                    $day[$dt[$key]]['quantity'] += 1;
                }else{
                    $day[$dt[$key]]['quantity'] = 1;
                }
            }

	    return $day;
	}
}