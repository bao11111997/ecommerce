<?php 
namespace App\Util;

/**
 * 
 */
class OrderCommon
{
	public static function calculate($orders, $day, $key)
	{
		foreach ($orders as $order) {
	        $time = strtotime($order->created_at);
	        $dt = getdate($time);
	            
	        if (isset($day[$dt[$key]]))
	        {
	            $day[$dt[$key]]['quantity'] += 1;

	            foreach ($order->orderDetails as $orderDetail) {
	                $day[$dt[$key]]['price'] += $orderDetail->price * $orderDetail->quantity;
	        }
	          
	        }else{
	            $day[$dt[$key]]['quantity'] = 1;
	            $day[$dt[$key]]['price'] = 0;
	            foreach ($order->orderDetails as $orderDetail) {
	                $day[$dt[$key]]['price'] += $orderDetail->price * $orderDetail->quantity;
	            }
	        }
	    }

	    return $day;
	}
}