<?php 
namespace App\Util;

use Illuminate\Support\Facades\File;
/**
 * 
 */
class ProductCommon
{
	public static function uploadFile($file, $folder)
    {
        // Get Extension (.png, jpg, jpeg, ...)
        $extension = $file->extension();

        // Convert string to lowercase
        $extension = strtolower($extension);

        // Set fileName
        $fileName = time() . '.' . $extension;

        // Upload file to server
        $file->move($folder, $fileName);

        // Get filePath
        $thumbnail = $folder . $fileName;

        return $thumbnail;
    }

    public static function uploadMultipleFile($file, $folder, $product_id)
    {
        $i = 1;

        foreach ($file as $product_image) {
            // Get Extension (.png, jpg, jpeg, ...)
            $extension = $product_image->extension();

            // Convert string to lowercase
            $extension = strtolower($extension);

            // Set fileName
            $fileName = time() . '.' . $i . '.' . $extension;

            // Upload file to server
            $product_image->move($folder, $fileName);

            // Get filePath
            $product_image = $folder . $fileName;

            $images[$i] = [
                'product_id' => $product_id,
                'url' => $product_image,
            ];

            $i++;
        }

        return $images;
    }

    public static function removeFile($files)
    {
        // Check Parameter
        if (empty($files)) {
            return false;
        }
        if (File::exists(public_path($files))) {
            File::delete(public_path($files));
        }

        // Return
        return true;
    }
}