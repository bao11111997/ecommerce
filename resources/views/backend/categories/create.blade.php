@extends('backend.layouts.master')

@section('title', 'Tạo Danh Mục Mới')

@section('content')
<div class="card card-danger">
      <div class="card-header">
        <h3 class="card-title">Nhập Tên Danh Mục</h3>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.categories.store') }}" method="post">
        @csrf
        <div class="row">
          <div class="col-4">
            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Tên Danh Mục">
          </div>
          <div class="col-4">
            <button type="submit" class="btn btn-success float-left">Thêm Mới</button>
          </div>
        </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
@endsection