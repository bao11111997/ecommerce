@extends('backend.layouts.master')

@section('title', 'Chỉnh Sửa Danh Mục')

@section('content')
<div class="card card-danger">
      <div class="card-header">
        <h3 class="card-title">Thay Đổi Tên Danh Mục</h3>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.categories.update', $category->id) }}" method="post">
        @csrf
        @method('put')
        <div class="row">
          <div class="col-4">
            <input type="text" name="name" value="{{ $category->name }}" class="form-control" placeholder="Tên Danh Mục">
          </div>
          <div class="col-4">
            <button type="submit" class="btn btn-success float-left">Cập Nhật Thay Đổi</button>
          </div>
        </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
@endsection