@extends('backend.layouts.master')

@section('title', 'Quản Lý Danh Mục')

@section('content')
@if (!empty($categories))
	<div class="card card-danger">
      <div class="card-header">
        <h3 class="card-title">Tìm Danh Mục</h3>

      </div>
      <div class="card-body">
        <form action="">
        <div class="row">
          <div class="col-4">
            <input type="text" name="search" value="{{ old('search') }}" class="form-control" placeholder="Tên Danh Mục">
          </div>
          <div class="col-8">
           
            
            <div class="float-left">
            	 <button type="submit" class="btn btn-success">Tìm Danh Mục</button> |
            	<a href="{{ route('admin.categories.index') }}" class="btn btn-secondary ">Đặt Lại</a> |
            	<a href="{{ route('admin.categories.create') }}" class="btn btn-info">Thêm Mới</a>
            	@include('errors.error')
            </div>
            	
          </div>
        </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
	<div class="card">
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          Tên Danh Mục
                      </th>
                      <th>
                          Danh Sách Sản Phẩm
                      </th>
                      <th>
                          Thao Tác
                      </th>
                  </tr>
              </thead>
              <tbody>
              	@php
                  		$i = 1;
                  	@endphp
                  	@foreach ($categories as $category)
                  <tr>
                  	<td>{{ $i }}</td>
                      <td>{{ $category->name }}</td>
                      <td>
                      	<a class="btn btn-primary btn-sm {{ count($category->products) === 0 ? 'disabled' : '' }}" href="{{ route('admin.products.index', ['category_id' => $category->id]) }}">
                            <i class="fas fa-folder"></i> Hiện có {{count($category->products)}} sản phẩm 
                        </a>
                      </td>
                      <td class="project-actions text-left row">
                        <a class="btn btn-info btn-sm" href="{{ route('admin.categories.edit', $category->id) }}">
                            <i class="fas fa-pencil-alt"></i> Chỉnh Sửa
                        </a> | 
                        <form action="{{ route('admin.categories.destroy', $category->id ) }}" method="post">
	                      @csrf
	                      @method('delete')

	                      <button type="submit" class="btn btn-danger btn-sm" {{ count($category->products) > 0 ? 'disabled' : '' }}><i class="fas fa-trash"></i> Xóa</button>
	                    </form>
                      </td>
                  </tr>
                  @php
                  	$i++;
                  @endphp
                @endforeach  
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
     </div>
@endif
@endsection