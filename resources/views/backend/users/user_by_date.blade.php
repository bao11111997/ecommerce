@extends('backend.layouts.master')

@section('title' ,'Tìm Kiếm Khách Hàng Theo Ngày')

@section('content')
	<form action="{{ route('admin.users.search-user-by-date') }}" method="get">
		<div class="form-group">
	      <label>Ngày</label>
	      <select class="form-control select2 select2-hidden-accessible" name="day" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
	        <option selected="selected" data-select2-id="3"></option>
	        @for ($i = 1; $i <= 31 ; $i++)
	        	
	        <option value="{{ $i }}">{{ $i }}</option>

	        @endfor
	      </select>
	  </div>
	    <div class="form-group">
	      <label>Tháng</label>
	      <select class="form-control select2 select2-hidden-accessible" name="month" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
	        <option selected="selected" data-select2-id="3"></option>
	        @for ($i = 1; $i <= 12 ; $i++)
	        	
	        <option value="{{ $i }}">{{ $i }}</option>

	        @endfor
	      </select>
	  </div>
	    <div class="form-group">
	      <label>Năm</label>
	      <select class="form-control select2 select2-hidden-accessible" name="year" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
	        <option selected="selected" value="{{ $year }}" data-select2-id="3">{{ $year }}</option>
	        @for ($i = $year - 1; $i >= $year - 20 ; $i--)
	        	
	        <option value="{{ $i }}">{{ $i }}</option>

	        @endfor
	      </select>
	    </div>

	    <div class="card-footer">
          <button type="submit" class="btn btn-primary">Tìm Kiếm</button>
        </div>
    </form>
               
@endsection
