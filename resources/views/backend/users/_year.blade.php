<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{ $users['year'] }}</h3>
  </div>
</div>

<div class="row">
  
  @foreach ($users as $month => $value)
    @continue($month == 'year')

  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>Khách Hàng: {{ $value['quantity'] }}</h3>
      </div>
      <div class="icon">
        <i class="ion ion-bag">{{ $month }}</i>
      </div>
      <a href="{{ route('admin.users.search-user-by-date', ['month' => $month, 'year' => $users['year']]) }}" class="small-box-footer">Thêm Thông Tin <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>

  @endforeach

</div>

