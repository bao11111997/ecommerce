@extends('backend.layouts.master')

@section('title', 'Danh Sách Khách Hàng')

@section('content')
<div class="row">
  <div class="col-md-3">
	<div class="card card-primary">
      <!-- form start -->
      <form method="get" action="{{ route('admin.users.index') }}">
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">ID Khách Hàng</label>
            <input type="number" name="customer_id" value="{{ request()->get('customer_id') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Customer ID">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" value="{{ request()->get('email') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Tên</label>
            <input type="text" name="name" value="{{ request()->get('name') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Số Điện Thoại</label>
            <input type="text" name="phone" value="{{ request()->get('phone') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Địa Chỉ</label>
            <input type="text" name="address" value="{{ request()->get('address') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Address">
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Tìm Kiếm</button>
          <a style="float: right;" class="btn btn-primary" href="{{ route('admin.users.index') }}">Xóa Bộ Lọc</a>
        </div>
      </form>
    </div>
  </div>

  <div class="col-md-9">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Danh Sách Khách Hàng</h3>
      </div>
      @if (!empty($users->all()))
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tên</th>
              <th>Email</th>
              <th>Số Điện Thoại</th>
              <th>Địa Chỉ</th>
              <th>Số Đơn Hàng</th>
            </tr>
          </thead>
          <tbody>
          	
          	@foreach ($users as $user)

            <tr>
              <td>{{ $user->id }}</td>
              <td><a href="{{ route('admin.users.show', ['id' => $user->id ]) }}">{{ $user->name }}</a></td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->phone }}</td>
              <td>{{ $user->address }}</td>
              <td>{{ count($user->orders) }}</td>
            </tr>

            @endforeach
          	
          </tbody>
        </table>
        <div>
          {{ $users->links('pagination::bootstrap-4') }}
        </div>
      </div>
      <!-- /.card-body -->
      @else
        <p>Không Có Khách Hàng</p>
        @endif
    </div>
    </div>
</div>
@endsection