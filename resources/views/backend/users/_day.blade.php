@if (!empty($users->all()))
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $date['day'] }} /
          <a href="{{ route('admin.users.search-user-by-date', ['month' => $date['month'], 'year' => $date['year']]) }}">{{ $date['month'] }}</a> /
          <a href="{{ route('admin.users.search-user-by-date', ['year' => $date['year']]) }}">{{ $date['year'] }}</a></h3>
  
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 500px;">
        <table class="table table-head-fixed text-nowrap">
           <thead>
            <tr>
              <th>ID</th>
              <th>Tên</th>
              <th>Email</th>
              <th>Số Điện Thoại</th>
              <th>Địa Chỉ</th>
              <th>Số Đơn Hàng</th>
            </tr>
          </thead>
          <tbody>
            
            @foreach ($users as $user)

            <tr>
              <td>{{ $user->id }}</td>
              <td><a href="{{ route('admin.users.show', ['id' => $user->id ]) }}">{{ $user->name }}</a></td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->phone }}</td>
              <td>{{ $user->address }}</td>
              <td>{{ count($user->orders) }}</td>
            </tr>

            @endforeach
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endif