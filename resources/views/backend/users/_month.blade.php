
<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{ $users['month'] }} /
      <a href="{{ route('admin.users.search-user-by-date', ['year' => $users['year']]) }}">{{ $users['year'] }}</a>
    </h3>
  </div>
</div>
<div class="row">

  @foreach ($users as $day => $value)
    @continue($day == 'year')
    @continue($day == 'month')

  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>Khách Hàng: {{ $value['quantity'] }}</h3>
      </div>
      <div class="icon">
        <i class="ion ion-bag">{{ $day }}</i>
      </div>
      <a href="{{ route('admin.users.search-user-by-date', ['day' => $day, 'month' => $users['month'], 'year' => $users['year']]) }}" class="small-box-footer">Thêm Thông Tin <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>

  @endforeach

</div>