@extends('backend.layouts.master')

@section('title', 'Thông Tin Khách Hàng')

@section('content')
	<div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('backend/admin_lte/dist/img/user4-128x128.jpg') }}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ $user->name }}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{ $user->email }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Số Điện Thoại</b> <a class="float-right">{{ $user->phone }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Địa Chỉ</b> <a class="float-right">{{ $user->address }}</a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Đơn Hàng</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="activity">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 400px;">
                          <table class="table table-head-fixed text-nowrap">
                            <thead>
                              <tr>
                                <th>Số Đơn Hàng</th>
                                <th>Tình Trạng</th>
                              </tr>
                            </thead>
                            <tbody>
                              
                              @foreach ($user->orders as $order)

                              <tr>
                                <td><a href="{{ route('admin.orders.show', ['id' => $order->id ]) }}">{{ $order->id }}</a></td>
                                @if ($order->status === 1)
                                  <td>Đang Xử Lý</td>
                                @elseif ($order->status === 2)
                                  <td>Đang Giao</td>
                                @elseif ($order->status === 3)
                                  <td>Đã Giao</td>
                                @else
                                  <td>Đã Hủy</td>
                                @endif
                                
                              </tr>

                              @endforeach
                              
                            </tbody>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>

                  </div>
                  <!-- /.tab-pane -->
                  
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>	
	
@endsection