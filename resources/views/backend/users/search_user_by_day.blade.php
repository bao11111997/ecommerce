@extends('backend.layouts.master')

@section('title', 'Tìm Kiếm Khách Hàng Theo Ngày')

@section('content')
	@if ($status === 'year')

		@include('backend.users._year')

	@elseif ($status === 'month')

		@include('backend.users._month')

	@elseif ($status === 'day')

		@include('backend.users._day')

	@endif
@endsection