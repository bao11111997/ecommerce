@extends('backend.layouts.master')
@section('title', 'Thông Tin Admin')
@section('content')
	<div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('backend/admin_lte/dist/img/user4-128x128.jpg') }}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ $admin->name }}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{ $admin->email }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Số Điện Thoại</b> <a class="float-right">{{ $admin->phone }}</a>
                  </li>
                  <li class="list-group-item">
                    @include('errors.error')
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Sản Phẩm Đã Thêm</a></li>
            @if (auth('admin')->user()->id == $admin->id || (auth('admin')->user()->role_id == 1 && $admin->role_id != 1))
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
            @endif
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="activity">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height: 400px;">
                          <table class="table table-head-fixed text-nowrap">
                            <thead>
                              <tr>
                                <th>Tên Sản Phẩm</th>
                                <th>Giá</th>
                              </tr>
                            </thead>
                            <tbody>
                    @foreach ($admin->products as $product)
                              <tr>
                                <td><a href="{{ route('admin.products.show', ['id' => $product->id ]) }}">{{ $product->name }}</a></td>
                                  <td>{{ $product->price }}</td>
                              </tr>
                    @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                  <div class="tab-pane" id="settings">
                    <form action="{{ route('admin.update', ['id' => $admin->id]) }}" method="post" class="form-horizontal">
                      @csrf
                      @method('put')
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ $admin->email }}" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="name" class="form-control" id="inputName" placeholder="Name" value="{{ $admin->name }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" name="email" class="col-sm-2 col-form-label">Số Điện Thoại</label>
                        <div class="col-sm-10">
                          <input type="number" name="phone" class="form-control" id="inputEmail" placeholder="Email" value="{{ $admin->phone }}">
                        </div>
                      </div>
                  @if (auth('admin')->user()->id != $admin->id && auth('admin')->user()->role_id == 1)
                      <div class="form-group row">
                        <label for="inputEmail" name="email" class="col-sm-2 col-form-label">Số Điện Thoại</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="role_id">
                            <option selected="selected" data-select2-id="3" value="{{ $admin->role->id }}">{{ $admin->role->role }}</option>
                    @foreach ($roles as $role)
                      @continue($role->id == 1)
                      @continue($role->id == auth('admin')->user()->role_id)
                                <option value="{{ $role->id }}">{{ $role->role }}</option>
                    @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" name="email" class="col-sm-2 col-form-label">Có Đang Hoạt Động</label>
                        <div class="col-sm-10">
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="status" {{ $admin->status ? 'checked' : '' }} value="1">
                            <label for="customRadio1" class="custom-control-label">Hoạt Động</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" {{ $admin->status ? '' : 'checked' }} id="customRadio2" name="status" value="0">
                            <label for="customRadio2" class="custom-control-label">Không Hoạt Động</label>
                          </div>
                        </div>
                      </div>
                  @endif
                      <div class="form-group row">
                        <label for="inputEmail" name="email" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                          <input type="password" name="password" class="form-control" id="inputEmail" placeholder="Nhập Password">
                        </div>
                  @error('password')
                    <span class="text-danger">{{ $message }}</span>
                  @enderror
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" name="email" class="col-sm-2 col-form-label">Nhập Lại Password</label>
                        <div class="col-sm-10">
                          <input type="password" name="password_confirmation" class="form-control" id="inputEmail" placeholder="Nhập Lại Password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Thay Đổi</button>
                          <a style="float: right;" class="btn btn-danger" href="{{ route('admin.index') }}">Continues</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection