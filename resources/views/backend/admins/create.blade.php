@extends('backend.layouts.master')

@section('title', 'Tạo Mới Quản Trị Viên')

@section('content')

<div class="card">
  <div class="card-body register-card-body">
    <p class="login-box-msg">Tạo Mới Admin</p>
    <form action="{{ route('admin.store') }}" method="post">
  @csrf
      <div class="form-group">
        <label for="inputSpentBudget">Tên Quản Trị Viên<span class="text-danger">(*)</span></label>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="name" placeholder="Nhập Tên" value="{{ old('name') }}">
          <div class="input-group-append">
            <div class="input-group-text">
        @error('name')
        		  <span class="text-danger">{{ $message }}</span>
        @enderror
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Email Quản Trị Viên<span class="text-danger">(*)</span></label>
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" placeholder="Nhập Email" value="{{ old('email') }}">
          <div class="input-group-append">
            <div class="input-group-text">
          @error('email')
        		  <span class="text-danger">{{ $message }}</span>
        	@enderror
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Số Điện Thoại<span class="text-danger">(*)</span></label>
        <div class="input-group mb-3">
          <input type="number" class="form-control" name="phone" placeholder="Nhập Số Điện Thoại" value="{{ old('phone') }}">
          <div class="input-group-append">
            <div class="input-group-text">
          @error('phone')
        			<span class="text-danger">{{ $message }}</span>
        	@enderror
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Chỉ Định Quyền<span class="text-danger">(*)</span></label>
        <select class="form-control" name="role">
          <option selected="selected" data-select2-id="3"></option>
        @foreach ($roles as $role)
          @continue($role->id == 1)
            <option value="{{ $role->id }}">{{ $role->role }}</option>
        @endforeach
        </select>
        @error('role')
          <div class="text-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Có Thể Sử Dụng<span class="text-danger">(*)</span></label>
        <div class="custom-control custom-radio">
          <input class="custom-control-input" type="radio" id="customRadio1" name="status" value="1">
          <label for="customRadio1" class="custom-control-label">Hoạt Động</label>
        </div>
        <div class="custom-control custom-radio">
          <input class="custom-control-input" type="radio" id="customRadio2" name="status" checked=""  value="0">
          <label for="customRadio2" class="custom-control-label">Không Hoạt Động</label>
        </div>
        @error('status')
          <div class="text-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Password<span class="text-danger">(*)</span></label>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
        @error('password')
        			<span class="text-danger">{{ $message }}</span>
        @enderror
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="inputSpentBudget">Nhập Lại Password<span class="text-danger">(*)</span></label>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Nhập lại password" name="password_confirmation">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
      </div>
      <p><i><span class="text-danger">(*)</span> Các Trường Bắt Buộc Phải Nhập</i></p>
      <div class="row">
        <!-- /.col -->
        <div class="col-3">
          <button type="submit" class="btn btn-primary btn-block">Tạo Tài Khoản</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
    <!-- /.form-box -->
</div>
@endsection