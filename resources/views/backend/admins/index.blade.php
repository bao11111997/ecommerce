@extends('backend.layouts.master')

@section('title', 'Danh Sách Quản Trị Viên')

@section('content')
<div class="row">
	<div class="col-md-3">
		<div class="card card-primary">
	      <!-- form start -->
	      <form method="get" action="{{ route('admin.index') }}">
	        <div class="card-body">
	          	<div class="form-group">
	            	<label for="exampleInputEmail1">Tên Hoặc Email</label>
	            	<input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter">
	          	</div>
	        
	        	<div class="form-group">
		        	 <label for="exampleInputEmail1">Tình Trạng</label>
			        <div class="form-check">
			          <input class="form-check-input" value="1" type="radio" name="active">
			          <label class="form-check-label">Hoạt Động</label>
			        </div>
			        <div class="form-check">
			          <input class="form-check-input" value="0" type="radio" name="active">
			          <label class="form-check-label">Không Hoạt Động</label>
			        </div>
		      </div>
		     </div>
	        <div class="card-footer">
	          <button type="submit" class="btn btn-primary">Tìm Kiếm</button>
	        </div>
	      </form>
	    </div>
	</div>
	<div class="col-md-9">
		<div class="card">
	      <div class="card-header">
	        <h3 class="card-title">Danh Sách Admin</h3>
	      </div>
	      <!-- /.card-header -->
	      <div class="card-body table-responsive p-0" style="height: 500px;">
	        <table class="table table-head-fixed text-nowrap">
	          <thead>
	            <tr>
	              <th>Admin ID</th>
	              <th>Tên</th>
	              <th>Số Điện Thoại</th>
	              <th>Email</th>
	              <th>Quyền</th>
	              <th>Trạng Thái</th>
	            </tr>
	          </thead>
	          <tbody>
	          	
	          	@foreach ($admins as $admin)
	          	@continue(auth('admin')->user()->id == $admin->id)
	            <tr>
	              <td>{{ $admin->id }}</td>
	              <td><a href="{{ route('admin.show', ['id' => $admin->id]) }}">{{ $admin->name }}</a></td>
	              <td>{{ $admin->phone }}</td>
	              <td>{{ $admin->email }}</td>
	              <td>{{ $admin->role->role }}</td>
	              <td>
	              	@if ($admin->active())
	              		Đang Hoạt Động
	              	@else
	              		Không Hoạt Động
	              	@endif
	              </td>
	            </tr>

	            @endforeach
	          	
	          </tbody>
	        </table>
	        <div>
	          {{ $admins->links('pagination::bootstrap-4') }}
	        </div>
	      </div>
	      <!-- /.card-body -->
	    </div>

	</div>
</div>
@endsection