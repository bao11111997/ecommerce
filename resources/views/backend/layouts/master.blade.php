<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title', 'Dashboard')</title>
    {{-- css --}}
    @include('backend.layouts.css')
</head>
<body class="hold-transition layout-top-nav">
    <div class="wrapper">
    
    {{-- navbar --}}
    @include('backend.layouts.navbar')

    {{-- header --}}
    @include('backend.layouts.header')

    <!-- Main content -->
    <div class="content">
      <div class="container">
            @yield('content')
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    {{-- footer --}}
    @include('backend.layouts.footer')
</div>
    {{-- js --}}
    @include('backend.layouts.js')
</body>
</html>
