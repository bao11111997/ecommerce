<!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="{{ route('admin.dashboard') }}" class="navbar-brand">
        <img src="{{asset('backend/admin_lte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}" class="nav-link">Trang Chủ</a>
          </li>
    @if (auth('admin')->user()->role_id == 1 || auth('admin')->user()->role_id == 2)
          
	         <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Đơn Hàng</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              	<li><a href="{{ route('admin.orders.index') }}" class="dropdown-item">Danh Sách Đơn Hàng</a></li>
              	<li><a href="{{ route('admin.orders.order-by-date') }}" class="dropdown-item">Đơn Hàng Theo Ngày</a></li>
            		<li><a href="{{ route('admin.orders.pending') }}" class="dropdown-item">Đơn Hàng Chưa Xử Lý</a></li>
            		<li><a href="{{ route('admin.orders.credit') }}" class="dropdown-item">Báo Cáo Tài Chính</a></li>
            		<li><a href="{{ route('admin.orders.summary') }}" class="dropdown-item">Báo Cáo Tóm Tắt Đơn Hàng</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Khách Hàng</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('admin.users.index') }}" class="dropdown-item">Danh Sách Khách Hàng</a></li>
                <li><a href="{{ route('admin.users.user-by-date') }}" class="dropdown-item">Khách Hàng Theo Ngày</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Danh Mục</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('admin.categories.index') }}" class="dropdown-item">Danh Sách Danh Mục</a></li>
                <li><a href="{{ route('admin.categories.create') }}" class="dropdown-item">Tạo Danh Mục</a></li>
            </ul>
          </li>
        @if (auth('admin')->user()->role_id == 1)
          
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Quản trị viên</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('admin.index') }}" class="dropdown-item">Danh Sách Quản trị Viên</a></li>
                <li><a href="{{ route('admin.create') }}" class="dropdown-item">Tạo Quản Trị Viên</a></li>
            </ul>
          </li>
        @endif
    @endif
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Sản Phẩm</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('admin.products.index') }}" class="dropdown-item">Danh Sách Sản Phẩm</a></li>
                <li><a href="{{ route('admin.products.create') }}" class="dropdown-item">Tạo Sản Phẩm</a></li>
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form method="get" action="{{ route('admin.products.index') }}" class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" name="search" placeholder="Nhập tên sản phẩm" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>

      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">{{ auth('admin')->user()->name }}</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="{{ route('admin.profile', ['id' => auth('admin')->user()->id]) }}" class="dropdown-item">Profile</a></li>
                <li><a href="{{ route('admin.logout') }}" class="dropdown-item">Đăng Xuất</a></li>
            </ul>
          </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->