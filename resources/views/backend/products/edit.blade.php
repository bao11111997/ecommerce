@extends('backend.layouts.master')

@section('title', 'Cập Nhật Sản Phẩm')

@section('content')

    <form action="{{ route('admin.products.update', $product->id) }}" method="post" enctype="multipart/form-data">
      @csrf
      @method('put')
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Tên Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="text" name="name" id="inputName" value="{{ $product->name }}" class="form-control">
              </div>
              <div class="form-group">
                <label for="inputDescription">Mô Tả Sản Phẩm<span class="text-danger">(*)</span></label>
                <textarea id="inputDescription" value="{{ $product->content }}" name="content" class="form-control" rows="4">{{ $product->content }}</textarea>
              </div>
              <div class="form-group">
                <label for="inputDescription">Loại Sản Phẩm<span class="text-danger">(*)</span></label>
                @foreach ($categories as $category)
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="{{ $category->id }}" name="category_id" {{ $product->category_id == $category->id ? 'checked' : ''}}>
                  <label class="form-check-label">{{ $category->name }}</label>
                </div>
                @endforeach
              </div>
              <div class="form-group">
                <label for="inputDescription">Thương Hiệu Sản Phẩm<span class="text-danger">(*)</span></label>
                @foreach ($brands as $brand)
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="{{ $brand->id }}" name="brand_id" {{ $product->brand_id == $brand->id ? 'checked' : ''}}>
                  <label class="form-check-label">{{ $brand->name }}</label>
                </div>
                @endforeach
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-body">
              <div class="form-group">
                <label for="inputClientCompany">Hình Đại Diện Sản Phẩm<span class="text-danger">(*)</span></label>
                <div class="custom-file">
                  <input type="file" name="thumbnail" class="custom-file-input" id="customFile">
                  <label class="custom-file-label">Choose file</label>
                </div>
                <img src="{{asset($product->thumbnail)}}" alt="{{ $product->name }}" style="width: 100%;">
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Hình Ảnh Sản Phẩm<span class="text-danger">(*)</span></label>
                <div class="custom-file" style="margin-bottom: 5px;">
                  <input type="file" name="product_images[]" class="custom-file-input" id="customFile" multiple>
                  <label class="custom-file-label">Choose file</label>
                </div>
                @foreach ($product->productImages as $productImage)
                  <input class="form-check-input" value="{{ $productImage->id }}" name="check_images[]" type="checkbox" style="margin-left: 0%;" checked>
                  <img src="{{asset($productImage->url)}}" style="width: 20%; margin-right: 6%; border: 1px solid red;">
                @endforeach
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Số Lượng Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="number" value="{{ $product->quantity }}" name="quantity" id="inputSpentBudget" class="form-control">
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Giá Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="number" value="{{ $product->price }}" name="price" id="inputEstimatedDuration" class="form-control">
              </div>
              <p><i><span class="text-danger">(*)</span> Phải Điền Vào Trường Này</i></p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <input type="submit" value="Cập Nhật Sản Phẩm" class="btn btn-success float-right">
        </div>
      </div>
    </form>
@endsection

@push('js')
  

<!-- bs-custom-file-input -->
<script src="{{ asset('backend/admin_lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- Page specific script -->
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
@endpush