@extends('backend.layouts.master')

@section('title', 'Danh Sách Sản Phẩm')

@section('content')
@if (!empty($products))
  @if ( Route::currentRouteName() === 'admin.products.index')
    <div class="card card-danger">
      <div class="card-header">
        <h3 class="card-title">Tìm Sản Phẩm</h3>
      </div>
      <div class="card-body">
        <form action="">
        <div class="row">
          <div class="col-4">
            <input type="text" name="search" value="{{ old('search') }}" class="form-control" placeholder="Tên Sản Phẩm">
          </div>
          <div class="col-4">
            <select class="form-control select2 select2-hidden-accessible" name="category_id" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
              <option selected="selected" data-select2-id="3">Lựa Chọn Danh Mục</option>
              @foreach ($categories as $category)                 
              <option value="{{ $category->id }}">{{ $category->name }}</option>

              @endforeach
            </select>
          </div>
          <div class="col-4">
            <button type="submit" value="Tìm Sản Phẩm" class="btn btn-success float-left">Tìm Sản Phẩm</button> |
            <a href="{{ route('admin.products.index') }}" class="btn btn-secondary">Đặt Lại</a>
          </div>
        </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
  @endif
	<div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row">
          	@foreach ($products as $product)

            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                </div>
                <div class="card-body pt-0">
                  <div class="row">	
                    <div class="col-7">
                      <h2 class="lead"><b>{{ $product->name }}</b></h2>
                      <p class="text-muted text-sm"><b>Số lượng: </b>{{ $product->quantity }}</p>
                      <p class="text-muted text-sm"><b>Giá: </b>{{ $product->price }} </p>
                      <p class="text-muted text-sm"><b>Admin: </b>{{ $product->admin->name }}</p>
                      <p class="text-muted text-sm"><b>Số đơn hàng: </b>{{ count($product->orderDetails) }}</p>
                    </div>
                    <div class="col-5 text-center">
                      <img src="{{ asset($product->thumbnail) }}" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right btn-group">
                    <a href="{{ route('admin.products.show', $product->id) }}" class="btn btn-sm btn-primary">Chi Tiết
                    </a>
                    <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-sm btn-info">Cập Nhật
                    </a>
                    <form action="{{ route('admin.products.destroy', $product->id ) }}" method="post">
                      @csrf
                      @method('delete')
                      <button type="submit" class="btn btn-sm btn-danger" {{count($product->orderDetails) > 0 ? 'disabled' : ''}}>Xóa</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            @endforeach

          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul class="pagination justify-content-center m-0">
              {{ $products->links('pagination::bootstrap-4') }}
            </ul>

          </nav>
        </div>
        <!-- /.card-footer -->
      </div>
@endif
@endsection