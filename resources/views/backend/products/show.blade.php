@extends('backend.layouts.master')

@section('title', 'Chi Tiết Sản Phẩm')

@section('content')

	<div class="card card-solid">
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-sm-6">
              <div class="col-12">
                <img src="{{ asset($product->thumbnail) }}" class="product-image" alt="Product Image">
              </div>
              <div class="col-12 product-image-thumbs">

              	@foreach ($product->productImages as $productImage)
              	
                <div class="product-image-thumb"><img src="{{ asset($productImage->url) }}" alt="Product Image"></div>

                @endforeach

              </div>
            </div>
            <div class="col-12 col-sm-6">
              <h3 class="my-3">{{ $product->name }}</h3>
              <p>
                @include('errors.error')
              </p>
              <p>{{ $product->content }}</p>
              
              <hr>
              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-default text-center active">
                  Loại Sản Phẩm
                  <br>
                  <i> {{ $product->category->name }}</i>
                </label>
                <label class="btn btn-default text-center active">
                  Thương hiệu
                  <br>
                  <i> {{ $product->brand->name }}</i>
                </label>
              </div>

              <div class="bg-gray py-2 px-3 mt-4">
                <h2 class="mb-0">
                  Số lượng: {{ $product->quantity }}
                </h2>
                <h2 class="mb-0">
                  {{ number_format($product->price) }} đ
                </h2>
              </div>

              <div class="mt-4">
                <a href="{{ route('admin.products.edit', $product->id) }}">
                  <div class="btn btn-primary btn-lg btn-flat">

                    Chỉnh sửa sản phẩm
                  </div>
                </a>
              </div>

            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>

@endsection	

@push('js')
<script>
  $(document).ready(function() {
    $('.product-image-thumb').on('click', function () {
      var $image_element = $(this).find('img')
      $('.product-image').prop('src', $image_element.attr('src'))
      $('.product-image-thumb.active').removeClass('active')
      $(this).addClass('active')
    })
  })
</script>
@endpush