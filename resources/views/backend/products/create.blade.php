@extends('backend.layouts.master')

@section('title', 'Thêm Mới Sản Phẩm')

@section('content')

    <form action="{{ route('admin.products.store') }}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Tên Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="text" name="name" id="inputName" value="{{ old('name') }}" class="form-control">
              </div>
              <div class="form-group">
                <label for="inputDescription">Mô Tả Sản Phẩm<span class="text-danger">(*)</span></label>
                <textarea id="inputDescription" name="content" class="form-control" rows="4"></textarea>
              </div>
              <div class="form-group">
                <label for="inputDescription">Loại Sản Phẩm<span class="text-danger">(*)</span></label>
                @foreach ($categories as $category)
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="{{ $category->id }}" name="category_id">
                  <label class="form-check-label">{{ $category->name }}</label>
                </div>
                @endforeach
              </div>
              <div class="form-group">
                <label for="inputDescription">Thương Hiệu Sản Phẩm<span class="text-danger">(*)</span></label>
                @foreach ($brands as $brand)
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="{{ $brand->id }}" name="brand_id">
                  <label class="form-check-label">{{ $brand->name }}</label>
                </div>
                @endforeach
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-body">
              <div class="form-group">
                <label for="inputClientCompany">Hình Đại Diện Sản Phẩm<span class="text-danger">(*)</span></label>
                <div class="custom-file">
                  <input type="file" name="thumbnail" class="custom-file-input" id="customFile">
                  <label class="custom-file-label">Choose file</label>
                </div>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Hình Ảnh Sản Phẩm<span class="text-danger">(*)</span></label>
                <div class="custom-file">
                  <input type="file" name="product_images[]" class="custom-file-input" id="customFile" multiple>
                  <label class="custom-file-label">Choose file</label>
                </div>
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Số Lượng Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="number" value="{{ old('quantity') }}" name="quantity" id="inputSpentBudget" class="form-control">
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Giá Sản Phẩm<span class="text-danger">(*)</span></label>
                <input type="number" value="{{ old('price') }}" name="price" id="inputEstimatedDuration" class="form-control">
              </div>
              <p><i><span class="text-danger">(*)</span> Phải Điền Vào Trường Này</i></p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{ route('admin.products.create') }}" class="btn btn-secondary">Đặt Lại</a>
          <input type="submit" value="Tạo Sản Phẩm" class="btn btn-success float-right">
        </div>
      </div>
    </form>
@endsection

@push('js')
  

<!-- bs-custom-file-input -->
<script src="{{ asset('backend/admin_lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- Page specific script -->
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
@endpush