@extends('backend.layouts.master')

@section('title', 'Chi Tiết Đơn Hàng')

@section('content')
	@if (!empty($order->orderDetails))
	
	<div class="invoice p-3 mb-3">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <h4>
            <i class="fas fa-globe"></i> AdminLTE, Inc.
            <small class="float-right">Ngày Tạo Đơn: {{ $order->created_at }}</small>
          </h4>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Từ
          <address>
            <strong>Admin, Inc.</strong><br>
            Địa Chỉ: 795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Số Điện Thoại: (804) 123-5432<br>
            Email: info@almasaeedstudio.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>
              @if (!empty($order->user))
              <a href="{{ route('admin.users.show', ['id' => $order->user->id]) }}">{{ $order->name }}</a>
              @else
              {{ $order->name }}
              @endif

            </strong><br>
            Địa Chỉ: {{ $order->address }}<br>
            Số Điện Thoại: {{ $order->phone }}<br>
            Email: @if (!empty($order->user))
            	{{ $order->user->email }}
            	@else
            		0
            	@endif
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Số Đơn Hàng:</b> {{ $order->id }}<br>
          <b>Tài Khoản:</b> 
            @if (!empty($order->user))
              	{{ $order->user->id }}
          	@else
              		0
          	@endif <br>

          <b>Tình Trạng Đơn Hàng:</b>
            @if ($order->status == 1)
                Chưa Xử Lý
            @elseif ($order->status == 2)
                Đang Giao Hàng
            @elseif ($order->status == 3)
                Đã Giao Thành Công
            @else
                Đã Hủy
            @endif
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Số Lượng</th>
              <th>Tên Sản Phẩm</th>
              <th>Mô Tả</th>
              <th>Giá Bán</th>
              <th>Tổng</th>
            </tr>
            </thead>
            <tbody>
            	@php
            		$total = 0;
            	@endphp
            @foreach ($orderDetails as $orderDetail)
            	@php
            		$subTotal = $orderDetail->quantity * $orderDetail->price;
            		$total += $subTotal;

            		$price = $orderDetail->price;

            	@endphp
            <tr>
              <td>{{ $orderDetail->quantity }}</td>
              <td><a href="{{ route('admin.products.show', ['id' => $orderDetail->product->id]) }}" target = "_ blank">{{ $orderDetail->product->name }}</a></td>
              <td>{{ $orderDetail->product->content }}</td>
              <td>{{ number_format($price) }}</td>
              <td>{{ number_format($subTotal) }}</td>
            </tr>

            @endforeach

            </tbody>
            <tfoot> 
              <tr>
                <th colspan="4">Vận Chuyển:</th>
                <td>0</td>
              </tr>
              <tr>
                <th colspan="4">Tổng Số Tiền:</th>
                <td>{{ number_format($total) }}</td>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </div>

	@endif
@endsection