<div class="card">
  <div class="card-header">
    <h3 class="card-title">{{ $orders['year'] }}</h3>
  </div>
</div>

<div class="row">
  
  @foreach ($orders as $month => $value)
    @continue($month == 'year')

  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>Số Đơn Hàng: {{ $value['quantity'] }}</h3>

        <p>Tổng Số Tiền: {{ number_format($value['price']) }}</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag">{{ $month }}</i>
      </div>
      <a href="{{ route('admin.orders.search-order-by-date', ['month' => $month, 'year' => $orders['year']]) }}" class="small-box-footer">Thêm Thông Tin <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>

  @endforeach

</div>

