@extends('backend.layouts.master')

@section('title', 'Danh Sách Đơn Hàng')

@section('content')
<div class="row">

<div class="col-md-3">
	<div class="card card-primary">
      <!-- form start -->
      <form method="get" action="{{ route('admin.orders.index') }}">
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Số Đơn Hàng</label>
            <input type="number" name="order_number" value="{{ request()->get('order_number') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Order Number">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">ID Khách Hàng</label>
            <input type="number" name="customer_id" value="{{ request()->get('customer_id') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Customer ID">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Tên</label>
            <input type="text" name="name" value="{{ request()->get('name') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Số Điện Thoại</label>
            <input type="text" name="phone" value="{{ request()->get('phone') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Địa Chỉ</label>
            <input type="text" name="address" value="{{ request()->get('address') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Address">
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Tìm Kiếm</button>
          <a style="float: right;" class="btn btn-primary" href="{{ route('admin.orders.index') }}">Xóa Bộ Lọc</a>
        </div>
      </form>
    </div>
</div>

<div class="col-md-9">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Danh Sách Các Đơn Hàng</h3>
        <br>
        @include('errors.error')
      </div>
      
      @if (!empty($orders->all()))
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table">
          <thead>
            <tr>
              <th>Tên Khách Hàng</th>
              <th>Tổng Số Lượng</th>
              <th>Tổng Tiền</th>
              <th>Trạng Thái</th>
              <th>Thao Tác</th>
            </tr>
          </thead>
          <tbody>
          	
          	@foreach ($orders as $order)

            <tr>
              <td>{{ $order->name }}</td>
              @php
                $price    = 0;
                $quantity = 0;
              @endphp
              @foreach ($order->orderDetails as $orderDetail)
                @php
                  $quantity += $orderDetail->quantity;
                  $price    += $orderDetail->quantity*$orderDetail->price;
                @endphp
                
              @endforeach
              <td>{{ $quantity }}</td>
              <td>{{ number_format($price) }} đ</td>
              <form action="{{ route('admin.orders.update', ['id' => $order->id]) }}" method="post">
                @csrf
                @method('put')
              <td>
                <select class="form-control select2 select2-hidden-accessible" name="status" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                  <option selected="selected" data-select2-id="3" value="{{ $order->status }}">
                    @if ($order->status == 1)
                        Chưa Xử Lý
                    @elseif ($order->status == 2)
                        Đang Giao Hàng
                    @elseif ($order->status == 3)
                        Đã Giao Thành Công
                    @else
                        Đã Hủy
                    @endif
                  </option>
                  @for ($i = 1; $i <= 4 ; $i++)
                    @continue($order->status == $i)
                    <option value="{{ $i }}">
                      @if ($i == 1)
                          Chưa Xử Lý
                      @elseif ($i == 2)
                          Đang Giao Hàng
                      @elseif ($i == 3)
                          Đã Giao Thành Công
                      @else
                          Đã Hủy
                      @endif
                    </option>
                  @endfor
                </select> 
              </td>
              <td>
                <a href="{{ route('admin.orders.show', ['id' => $order->id ]) }}" class="btn btn-info" target = "_ blank">Chi tiết</a>|
                <button type="submit" class="btn btn-primary"
                  {{$order->status == 1 || $order->status == 2 ? '' : 'disabled'}}
                >Cập Nhật</button>
                </form>

                <br>
                
                <form action="{{ route('admin.orders.destroy', $order->id ) }}" method="post">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger"
                  {{$order->status == 1 ? '' : 'disabled'}}>Xóa</button>
                </form>
                
              </td>
              

              
            </tr>

            @endforeach
          	
          </tbody>
        </table>
        <div>
          {{ $orders->links('pagination::bootstrap-4') }}
        </div>
      </div>
      <!-- /.card-body -->
      @else
    <p>Không có đơn hàng</p>
    @endif
    </div>
    
  </div>
</div>
@endsection