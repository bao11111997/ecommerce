@extends('backend.layouts.master')

@section('title', 'Tìm Kiếm Đơn Hàng Theo Ngày')

@section('content')
	@if ($status === 'year')

		@include('backend.orders._year')

	@elseif ($status === 'month')

		@include('backend.orders._month')

	@elseif ($status === 'day')

		@include('backend.orders._day')

	@endif
@endsection