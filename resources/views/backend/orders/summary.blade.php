@extends('backend.layouts.master')

@section('title', 'Báo Cáo Tóm Tắt Đơn Hàng')

@section('content')

<div class="card-body">
	<form action="" method="get">
        <!-- Date dd/mm/yyyy -->
        <div class="form-group">
          <label>Ngày Băt Ðầu:</label>

          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" value="{{ request()->get('start') }}" name="start" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask="" inputmode="numeric">
          </div>
          <!-- /.input group -->
        </div>
        <div class="form-group">
          <label>Ngày Kết Thúc:</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" value="{{ request()->get('end') }}" name="end" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask="" inputmode="numeric">
          </div>
          <!-- /.input group -->
        </div>
        <div class="form-group">
          <label>Loại Sản Phẩm:</label>
          @foreach ($categories as $category)
          <div class="form-check d-inline">
            <input class="form-check-input" value="{{ $category->id }}" name="category[]" type="checkbox">
            <label class="form-check-label">{{ $category->name }}</label>
          </div>
          @endforeach
        </div>
        <div class="form-group">
          <label>Nhãn Hiệu:</label>
          @foreach ($brands as $brand)
          <div class="form-check d-inline">
            <input class="form-check-input" value="{{ $brand->id }}" name="brand[]" type="checkbox">
            <label class="form-check-label">{{ $brand->name }}</label>
          </div>
          @endforeach
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Nhận Báo Cáo</button>
          <a style="float: right;" class="btn btn-primary" href="{{ route('admin.orders.summary') }}">Xóa Bộ Lọc</a>
        </div>
	</form>
</div>


@if (isset($orderDetails) && !empty($orderDetails->all()) )

	<div class="card">
      <div class="card-header">
        <h3 class="card-title">Báo Cáo Tài Chính</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 500px;">
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th>Số Sản Phẩm</th>
              <th>Tên Sản Phẩm</th>
              <th>Số Lượng</th>
              <th>Tổng</th>
            </tr>
          </thead>
          <tbody>
          	@php
              $total = 0;
              
            @endphp
          	@foreach ($orderDetails as $orderDetail)
            <tr>
              <td>{{ $orderDetail->product_id }}</td>
              <td>{{ $orderDetail->product->name }}</td>
              <td>{{ $orderDetail->quantity }}</td>
              <td>{{ number_format($orderDetail->price) }}</td>
            </tr>
              @php
                $total += $orderDetail->price
              @endphp
            @endforeach
            
          </tbody>
          <tfoot>
            <tr>
              <td colspan="3"><b>Tổng Số Tiền</b></td>
              <td><b>{{ number_format($total) }}</b></td>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@elseif (isset($orderDetails))

<p>Không Có Báo Cáo</p>

@endif



@endsection

@push('js')
	<!-- Select2 -->
	<script src="{{ asset('backend/admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>

	<script src="{{ asset('backend/admin_lte/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
@endpush
