@if (!empty($orders->all()))
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $date['day'] }} /
          <a href="{{ route('admin.orders.search-order-by-date', ['month' => $date['month'], 'year' => $date['year']]) }}">{{ $date['month'] }}</a> /
          <a href="{{ route('admin.orders.search-order-by-date', ['year' => $date['year']]) }}">{{ $date['year'] }}</a></h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 400px;">
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th>Số Đơn Hàng</th>
              <th>Tên Khách Hàng</th>
              <th>Số Điện Thoại</th>
              <th>Địa Chỉ</th>
            </tr>
          </thead>
          <tbody>
          	@foreach ($orders as $order)

            <tr>
              <td><a href="{{ route('admin.orders.show', ['id' => $order->id ]) }}">{{ $order->id }}</a></td>
              <td>{{ $order->name }}</td>
              <td>{{ $order->phone }}</td>
              <td>{{ $order->address }}</td>
            </tr>

            @endforeach
          	
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endif