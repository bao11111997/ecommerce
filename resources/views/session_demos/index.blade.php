@extends('layouts.master')

{{-- set page title --}}
@section('title', 'Giỏ hàng của bạn')

@section('content')
    <section class="cart-info">
        <!-- show error messsage -->
        @include('errors.error')

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
               <li class="breadcrumb-item active" aria-current="page">Giỏ hàng của bạn</li>
            </ol>
        </nav>

        <h1>GIỎ HÀNG</h1>

        <hr>

        <h2>Add new a Product into Cart</h2>
        <div class="border p-5 mb-5">
            <form method="POST" action="{{ route('session-demo.store') }}">
                @csrf
                <div class="mb-3">
                    <label for="">Product ID</label>
                    <input type="text" readonly name="product_id" value="{{ (string) Str::orderedUuid() }}" class="form-control">
                    @error('product_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="mb-3">
                    <label for="">Quantity</label>
                    <input type="number" name="quantity" value="" class="form-control">
                    @error('quantity')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Submit to Add Product into Cart</button>
                </div>
            </form>
        </div>

        <hr>

        <h2 class="mt-5">Thông tin giỏ hàng</h2>
        @if(empty($cart_demos))
            <p class="text-danger">Giỏ hàng trống.</p>
        @else
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Quantity</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cart_demos as $cart)
                        <tr>
                            <td>{{ $cart['cart_product_id'] }}</td>
                            <td class="current-quantity">
                                <input type="number" name="quantity" value="{{ $cart['cart_quantity'] }}">
                            </td>
                            <td>
                                <form method="POST" action="{{ route('session-demo.update', ['id' => $cart['cart_product_id']]) }}" class="frm-cart-update-quantity">
                                    @csrf
                                    @method('PUT')
                                    
                                    <button type="submit" class="btn btn-danger">Update Quantity Product use AJAX</button>
                                </form>
                            </td>
                            <td>
                                <form method="POST" action="{{ route('session-demo.destroy', ['id' => $cart['cart_product_id']]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Remove Product</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="mt-5">
                <form method="POST" action="">
                    @csrf
                    <button type="submit" class="btn btn-danger">Làm trống giỏ hàng</button>
                </form>
            </div>
        @endif
    </section>
@endsection

@push('js')
    <script>
        /**
         * Update Quantity Of Product In Cart
         */
        $(document).on('submit', '.frm-cart-update-quantity', function (event) {
            // Block event submit
            event.preventDefault();

            /**
             * process form and send AJAX Data
             * Define variable
             */
            let csrf = $(this).find('input[name=_token]').val();
            let quantity = $(this).closest('tr').find('input[name=quantity]').val();
            let url = $(this).attr('action');

            // Validate quantity
            if (quantity <= 0) {
                // toastr.error('Quantity is greater 0.');
                alert('Quantity is greater 0.');
            } else {
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: {
                        _token: csrf,
                        quantity: quantity
                    },
                    success: function (response) {
                        // Show message success
                        // toastr.success('Cập nhật đơn hàng thành công.');
                        alert('Cập nhật đơn hàng thành công.');
                    },
                    error: function (err) {
                        // Display an error
                        if (err.responseJSON.message) {
                            // toastr.error(err.responseJSON.message);
                            alert(err.responseJSON.message);
                        } else {
                            // toastr.error('Cập nhật đơn hàng thất bại.');
                            alert('Cập nhật đơn hàng thất bại.');
                        }
                    },
                    dataType: 'json'
                });
            }
        });
    </script>
@endpush
