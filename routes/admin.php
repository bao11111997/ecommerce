<?php 

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;

// Forgot password
Route::get('admin/forgot-password', [AdminController::class, 'forgotPassword'])->name('admin.password.request');

// Send Mail
Route::post('admin/forgot-password', [AdminController::class, 'sendMail'])->name('admin.password.email');

// Reset Password
Route::get('admin/reset-password', [AdminController::class, 'resetPassword'])->name('admin.password.reset');

Route::post('admin/reset-password', [AdminController::class, 'updatePassword'])->name('admin.password.update');

Route::group(['middleware' => ['auth.admin'],'prefix' => 'admin', 'as' => 'admin.'], function() {
    
    Route::get('/', [ProductController::class, 'index'])->name('dashboard');

    // Get Form Login
    Route::get('login', [AdminController::class, 'login'])->name('login');

    // Check Login
    Route::post('login', [AdminController::class, 'checkLogin'])->name('check-login');

    // Logout
    Route::get('logout', [AdminController::class, 'logout'])->name('logout');


    // Get List Admin
    Route::get('index', [AdminController::class, 'index'])->name('index');

    // Get Form Create a Admin
    Route::get('create', [AdminController::class, 'create'])->name('create')->middleware('super.admin');

    // Add new Admin
    Route::post('store', [AdminController::class, 'store'])->name('store')->middleware('super.admin');

    // Show Admin
    Route::get('show/{id}', [AdminController::class, 'show'])->name('show')->middleware('super.admin');

    // Profile Admin
    Route::get('profile/{id}', [AdminController::class, 'profile'])->name('profile');

    // UpDate Admin
    Route::put('/{id}', [AdminController::class, 'update'])->name('update');

    // Route Orders
    Route::group(['middleware' => ['direct.admin'], 'prefix' => 'orders', 'as' => 'orders.'], function() {
        
        Route::get('/', [OrderController::class, 'index'])->name('index');
        Route::get('show/{id}', [OrderController::class, 'show'])->name('show');
        Route::put('update/{id}', [OrderController::class, 'update'])->name('update');
        Route::delete('delete/{id}', [OrderController::class, 'destroy'])->name('destroy');

        // Get Form Order By Date
        Route::get('order-by-date', [OrderController::class, 'orderByDate'])->name('order-by-date');

        // Get List Order By Date
        Route::get('search-order-by-date', [OrderController::class, 'searchOrderByDate'])->name('search-order-by-date');

        // Get Credit Report
        Route::get('credit', [ReportController::class, 'creditReport'])->name('credit');

        // Get Summary Report
        Route::get('summary', [ReportController::class, 'summaryReport'])->name('summary');

        // Get Pending Order
        Route::get('pending', [OrderController::class, 'pendingOrder'])->name('pending');

        
    });

    // Route Users
    Route::group(['middleware' => ['direct.admin'], 'prefix' => 'users', 'as' => 'users.'], function() {

        // Get List Users
        Route::get('/', [UserController::class, 'index'])->name('index');

        // Get Profile
        Route::get('show/{id}', [UserController::class, 'show'])->name('show');

        // Get Form User By Date
        Route::get('user-by-date', [UserController::class, 'userByDate'])->name('user-by-date');

        // Get List Order By Date
        Route::get('search-user-by-date', [UserController::class, 'searchUserByDate'])->name('search-user-by-date');
    });

    // Route Category
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function() {

        Route::get('/', [CategoryController::class, 'index'])->name('index');
        Route::get('create', [CategoryController::class, 'create'])->name('create');
        Route::post('/', [CategoryController::class, 'store'])->name('store');
        Route::get('show/{id}', [CategoryController::class, 'show'])->name('show');
        Route::get('edit/{id}', [CategoryController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [CategoryController::class, 'update'])->name('update');
        Route::delete('delete/{id}', [CategoryController::class, 'destroy'])->name('destroy');   
    });

    // Route Products
    Route::group(['prefix' => 'products', 'as' => 'products.'], function() {

        Route::get('/', [ProductController::class, 'index'])->name('index');
        Route::get('create', [ProductController::class, 'create'])->name('create');
        Route::post('/', [ProductController::class, 'store'])->name('store');
        Route::get('show/{id}', [ProductController::class, 'show'])->name('show');
        Route::get('edit/{id}', [ProductController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [ProductController::class, 'update'])->name('update');
        Route::delete('delete/{id}', [ProductController::class, 'destroy'])->name('destroy');

        // Route::get('delete/{id}', function($id) {
        //     return redirect(route('admin.products.show', $id))->with('error', 'Sản Phẩm Hiện Đang Có Đơn Hàng');
        // });
    });
});