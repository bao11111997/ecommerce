<?php

use App\Http\Controllers\CartDemoController;
use App\Http\Controllers\ProductDemoController;
use App\Http\Controllers\SessionDemoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

require __DIR__.'/admin.php';

/**
 * Route For Session Demo
 */
Route::group(['prefix' => 'session-demo', 'as' => 'session-demo.'], function () {
    // URL này để hiển thị tất cả data đang có trong SESSION
    Route::get('/', [SessionDemoController::class, 'index'])->name('index');

    // URL này để thêm mới 1 sản phẩm vào Giỏ Hàng
    Route::post('/', [SessionDemoController::class, 'store'])->name('store');

    // URL này để update quantity của 1 sản phẩm đã có trong Giỏ Hàng
    Route::put('/{id}', [SessionDemoController::class, 'update'])->name('update');

    // URL này để xoá 1 sản phẩm ra khỏi Giỏ Hàng
    Route::delete('/{id}', [SessionDemoController::class, 'destroy'])->name('destroy');

    // URL này để xoá tất cả sản phẩm ra khỏi Giỏ Hàng
    Route::delete('/all', [SessionDemoController::class, 'destroyAll'])->name('destroyAll');
});

// Route for Product Demo
Route::get('product-demo', [ProductDemoController::class, 'index'])->name('product-demo.index')->middleware('guest');
Route::get('product-demo/create', [ProductDemoController::class, 'create'])->name('product-demo.create')->middleware('guest');
Route::post('product-demo', [ProductDemoController::class, 'store'])->name('product-demo.store')->middleware('guest');

// Route for Cart Demo
Route::group(['prefix' => 'cart-demo', 'as' => 'cart-demo.', 'middleware' => 'guest'], function () {
    Route::get('/', [CartDemoController::class, 'index'])->name('index');
    Route::post('/{id}/add', [CartDemoController::class, 'addCart'])->name('add-cart');
    Route::put('/{id}/update-quantity', [CartDemoController::class, 'updateQuantity'])->name('update-quantity');
    Route::delete('/{id}/remove-product-in-cart', [CartDemoController::class, 'removeProductInCart'])->name('remove-product-in-cart');
});

